/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_run_subprocess.h>

#include <buildboxcommon_exception.h>
#include <buildboxcommon_temporaryfile.h>

#include <string>
#include <ostream>
#include <experimental/filesystem>
#include <chrono>
#include <cstdlib>
#include <sstream>
#include <stdexcept>
#include <functional>
#include <fstream>
#include <memory>

namespace buildboxcommon {
namespace buildboxrun {
namespace oci {
    const std::string RunSubprocess::stdoutLogPrefix = "buildboxrun_ocirunner_out";
    const std::string RunSubprocess::stderrLogPrefix = "buildboxrun_ocirunner_err";

    RunSubprocess::RunSubprocess(const FILESYSTEM::path &dir, const std::string &cmd, std::ostream* out, std::ostream* err)
        : stdoutLog(out ? std::make_unique<TemporaryFile>(stdoutLogPrefix.c_str()) : std::make_unique<TemporaryFile>()),
          stderrLog(err ? std::make_unique<TemporaryFile>(stderrLogPrefix.c_str()) : std::make_unique<TemporaryFile>()) {
        std::ostringstream cmdBuilder;

        cmdBuilder << "cd " << FILESYSTEM::absolute(dir) << " && " << cmd;

        if (out)
            cmdBuilder << " >" << stdoutLog->strname();

        if (err)
            cmdBuilder << " 2>" << stderrLog->strname();

        const std::string fullCmd = cmdBuilder.str();
        const int statusCode = std::system(fullCmd.c_str());

        if (out)
            std::ifstream(stdoutLog->strname()) >> out->rdbuf();

        if (err)
            std::ifstream(stderrLog->strname()) >> err->rdbuf();

        if (statusCode != 0)
            BUILDBOXCOMMON_THROW_EXCEPTION(
                std::runtime_error,
                "Command failed to execute: " << fullCmd << " with status code " << statusCode
            );
    }
}
}
}
