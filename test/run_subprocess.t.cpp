/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_run_subprocess.h>

#include <buildboxcommon_temporaryfile.h>

#include <experimental/filesystem>
#include <sstream>
#include <algorithm>
#include <string>

#include <gtest/gtest.h>

using namespace buildboxcommon;
using namespace buildboxrun::oci;

TEST(RunSubprocess, Success) {
    std::ostringstream out;
    {
        RunSubprocess h(FILESYSTEM::current_path(), "echo Hi >>_test && echo Hi", &out, nullptr);

        ASSERT_EQ(out.str(), std::string("Hi\n"));
        ASSERT_TRUE(FILESYSTEM::exists(FILESYSTEM::current_path() / "_test"));

        auto dirIt = FILESYSTEM::directory_iterator(TemporaryFileDefaults::DEFAULT_TMP_DIR);
        ASSERT_TRUE(std::any_of(
            FILESYSTEM::begin(dirIt),
            FILESYSTEM::end(dirIt),
            [](const FILESYSTEM::directory_entry &entry) {
                return FILESYSTEM::is_regular_file(entry.path()) && entry.path().filename().string().find("buildboxrun_ocirunner_out") != std::string::npos;
            }
        ));
        FILESYSTEM::remove(FILESYSTEM::current_path() / "_test");
    }

    auto dirIt = FILESYSTEM::directory_iterator(TemporaryFileDefaults::DEFAULT_TMP_DIR);
    ASSERT_FALSE(std::any_of(
        FILESYSTEM::begin(dirIt),
        FILESYSTEM::end(dirIt),
        [](const FILESYSTEM::directory_entry &entry) {
            return FILESYSTEM::is_regular_file(entry.path()) && entry.path().filename().string().find("buildboxrun_ocirunner_out") != std::string::npos;
        }
    ));
}

