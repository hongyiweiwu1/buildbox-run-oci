/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_oci.h>

#include <buildboxcommon_merklize.h>
#include <buildboxcommon_mergeutil.h>

#include <gtest/gtest.h>

#include <memory>
#include <vector>
#include <string>

using namespace buildboxcommon;
using namespace buildboxrun::oci;
using namespace build::buildgrid;

class OCIRunnerFixture : public OCIRunner, public ::testing::Test {};

TEST_F(OCIRunnerFixture, TestMakeDigest) {
    const std::string property_name = "chrootRootDigest";
    Platform platform;
    Platform_Property &property = *platform.add_properties();
    property.set_name(property_name);
    property.set_value("abc/1234");

    const auto digest = this->getBundleDigest(platform);
    ASSERT_EQ(digest.hash(), "abc");
    ASSERT_EQ(digest.size_bytes(), 1234);
}

TEST_F(OCIRunnerFixture, TestMakeDigest_InvalidAssetQualifier) {
    static const std::string assetQualifierPropName = "chrootAssetQualifier";

    Platform platform;
    Platform_Property &property = *platform.add_properties();
    property.set_name(assetQualifierPropName);
    property.set_value("abc/1234");

    EXPECT_THROW({ this->getBundleDigest(platform); }, std::runtime_error);
}

TEST_F(OCIRunnerFixture, TestMakeDigest_NoAssetUrl_FallbackToDigest) {
    static const std::string assetQualifierPropName = "chrootAssetQualifier";
    static const std::string digestPropName = "chrootRootDigest";

    Platform platform;
    Platform_Property &property = *platform.add_properties();
    property.set_name(assetQualifierPropName);
    property.set_value("abc/1234");
    Platform_Property &digestProperty = *platform.add_properties();
    digestProperty.set_name(digestPropName);
    digestProperty.set_value("abc/1234");

    const auto digest = this->getBundleDigest(platform);
    ASSERT_EQ(digest.hash(), "abc");
    ASSERT_EQ(digest.size_bytes(), 1234);
}
